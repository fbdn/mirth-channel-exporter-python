# Mirth Channel Exporter

_Simple python script that exports Mirth Channel statstics to Prometheus._

## Getting started
You will need `python3-pip` installed.

```bash
git clone https://gitlab.com/fbdn/mirth-channel-exporter-python.git && cd mirth-channel-exporter-python
python3 -m pip install -r requirements.txt
cp example.env .env
```

### Configuration 
You can set these variables either in your environment manually or in `./.env`:

| Name | Default | Description | 
|------|---------|-------------|
| EXPORTER_PORT | 9141 | Port on which the metrics exporter listens on |
| EXPORTER_POLL_INTERVAL | 1 | Waiting time between querying the Mirth API |
| MIRTH_ENDPOINT | http://localhost/api | Endpoint of your Mirth API |
| MIRTH_VERIFY_SSL | False | Verification of your mirth API's SSL certificate |
| MIRTH_USERNAME | admin | Username to login with |
| MIRTH_PASSWORD | admin | Password for the given username |
| DEBUG | False | Turn on debug mode for excessive output |



## Systemd service
Assuming you cloned the repo to `/opt/` you can simply copy `mirth-exporter.service` to you systemd folder.  
Otherwise just change the path in the service file.  

```bash
sudo cp mirth-exporter.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable --now mirth-exporter.service
```


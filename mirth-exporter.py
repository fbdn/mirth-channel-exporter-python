import asyncio, re, os
from dotenv import load_dotenv
from mirth_client import MirthAPI
from prometheus_client import Gauge
from prometheus_async.aio.web import start_http_server


load_dotenv()

EXPORTER_PORT = os.getenv("EXPORTER_PORT", "9141")
EXPORTER_POLL_INTERVAL = float(os.getenv("EXPORTER_POLL_INTERVAL", "1"))
MIRTH_ENDPOINT = os.getenv("MIRTH_ENDPOINT", "http://localhost/api")
MIRTH_USERNAME = os.getenv("MIRTH_USERNAME", "admin")
MIRTH_PASSWORD = os.getenv("MIRTH_PASSWORD", "admin")
MIRTH_VERIFY_SSL = False if os.getenv("MIRTH_VERIFY_SSL") == "false" else True
DEBUG = True if os.getenv("EXPORTER_DEBUG","false").lower() == "true" else False


async def main():
    server = await start_http_server(port=EXPORTER_PORT)
    print(f"Server running on port {EXPORTER_PORT}")

    async with MirthAPI(MIRTH_ENDPOINT, verify_ssl=MIRTH_VERIFY_SSL) as api:
        await api.login(MIRTH_USERNAME, MIRTH_PASSWORD)
        print(f"Connected to mirth endpoint {MIRTH_ENDPOINT}")

        channels = await api.channels()
        print(f"Found {len(channels)} channels")

        gauges = {}

        while True:
            # Get list of channels
            channels = await api.channels()
            for channel in channels:
                # Get channel info for human readable name
                metadata = await channel.get_info()
                s = await api.channel(metadata.id).get_statistics()
                print(metadata.name, s) if DEBUG else None

                # Loop through channel statistics
                for i in ["received", "sent", "error", "filtered", "queued"]:

                    # Create cleaned name as prometheus metric name
                    channel_name_cleaned = re.sub("\W+", "_", metadata.name)
                    metric_name = channel_name_cleaned + "_" + i + "_total"

                    # See if it is already registered
                    if i not in gauges:

                        # Create dict entry with Gauge object
                        gauges[i] = Gauge(i+"_total", i, ["mirth_channel"])
                    # Set Gauge current value
                    gauges[i].labels(channel_name_cleaned).set(getattr(s, i))

            print(f"Exporting {len(gauges)} metrics...") if DEBUG else None
            await asyncio.sleep(EXPORTER_POLL_INTERVAL)

asyncio.run(main())
